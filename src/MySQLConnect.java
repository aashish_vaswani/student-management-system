
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author HOME
 */
public class MySQLConnect {
    
    /*
    The below method creates a connection mysql database named as 'student_management' with the user name 'aashishvaswani' and Password 'anmolvaswani'.
    It returns the java.sql.Connection object if the connection was successfully established otherwise returns 'null'.
    */
 public static Connection getConnection(){
     try{
         Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/student_management","php","php");//(jdbc:subprotocol:subname, user, password)
         return conn;
     }catch(SQLException e){
         JOptionPane.showMessageDialog(null, "Connection Failed1" + e.getMessage());
         return null;
     }
 }
 /*
 public static void main(String args[]){
     Connection conn = getConnection();
     if(conn!=null){
         JOptionPane.showMessageDialog(null, "Connection Established!");
     }
 }
*/
}
